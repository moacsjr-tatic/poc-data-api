import logging
from eve import Eve
from eve.auth import BasicAuth

 #Authorization: Basic YWRtaW46cm9ibyNEZDNEY2s5MDc4a2RpMjAxOCQjIw==

class MyBasicAuth(BasicAuth):
    def check_auth(self, username, password, allowed_roles, resource,method):
        return username == 'admin' and password == 'robo#Dd3Dck9078kdi2018$##'

app = Eve(auth=MyBasicAuth)

def post_request_callback(items):
    logging.info('Post request ...%s', items)

app.on_inserted_requests = post_request_callback

if __name__ == '__main__':
    # enable logging to 'app.log' file
    handler = logging.FileHandler('app.log')

    # set a custom log format, and add request
    # metadata to each log line
    handler.setFormatter(logging.Formatter(
        '%(asctime)s %(levelname)s: %(message)s '
        '[in %(filename)s:%(lineno)d] -- ip: %(clientip)s, '
        'url: %(url)s, method:%(method)s'))

    # the default log level is set to WARNING, so
    # we have to explicitly set the logging level
    # to INFO to get our custom message logged.
    app.logger.setLevel(logging.DEBUG)

    # append the handler to the default application logger
    app.logger.addHandler(handler)

    # let's go
    app.run(host="0.0.0.0")

