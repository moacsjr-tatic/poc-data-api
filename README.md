# Pattern DATA-API #


El estándar data-api se usa para exponer el acceso a una base de datos a través de REST-API.

### Porque HTTP? ###

Hay tres características básicas que hacen que HTTP sea un protocolo simple pero poderoso:

* HTTP no tiene conexión: el cliente HTTP inicia una solicitud HTTP y después de que se realiza una solicitud, el cliente se desconecta del servidor y espera una respuesta. El servidor procesa la solicitud y restablece la conexión con el cliente para devolver la respuesta.

* HTTP es independiente de los medios: esto significa que HTTP puede enviar cualquier tipo de datos siempre que el cliente y el servidor sepan cómo manejar el contenido de los datos. Esto es necesario para que el cliente y el servidor especifiquen el tipo de contenido utilizando el tipo MIME apropiado.

* HTTP no tiene estado: como se mencionó anteriormente, HTTP no tiene conexión y este es un resultado directo de que HTTP es un protocolo sin estado. El servidor y el cliente se conocen entre sí solo durante una solicitud actual. Luego, ambos se olvidan el uno del otro. Debido a esta naturaleza del protocolo, ni el cliente ni el navegador pueden retener información entre diferentes solicitudes en las páginas web.

### Setup ###

* Clonar el projeto
* Acceda a la carpeta del proyecto a través de la terminal y ejecute el comando

`docker-compose up --build`

