db.auth('oms', 'omnicanal')

db = db.getSiblingDB('inventario')

db.createUser({
  user: 'oms',
  pwd: 'omnicanal',
  roles: [
    {
      role: 'root',
      db: 'admin',
    },
  ],
});

db.stocks.insert({
    "material": {
        "sku": "100201",
        "nombre": "IPHONE 64 BRANCO",
        "description": "Apple iPhone 11 con 64 GB, pantalla Retina HD de 6.1, iOS 13, cámara trasera doble de 12MP, resistente al agua y batería de larga duración - Blanco"
    },
    "disponibilidad": [
        {
            "centro": "C100",
            "nodo": false,
            "codigo_nodo": "C200",
            "ubicacion": {
                "codigo_municipio": "999",
                "departamiento": "999",
                "latitude": "0000000000",
                "longitude": "0000000000"
            }
        },{
            "centro": "C101",
            "nodo": false,
            "codigo_nodo": "C200",
            "ubicacion": {
                "codigo_municipio": "999",
                "departamiento": "999",
                "latitude": "0000000000",
                "longitude": "0000000000"
            }
        },{
            "centro": "C200",
            "nodo": false,
            "codigo_nodo": "C200",
            "ubicacion": {
                "codigo_municipio": "999",
                "departamiento": "999",
                "latitude": "0000000000",
                "longitude": "0000000000"
            }
        }
    ]
})

db.stocks.insert({
    "material": {
        "sku": "100202",
        "nombre": "IPHONE 128 BRANCO",
        "description": "Apple iPhone 11 con 64 GB, pantalla Retina HD de 6.1, iOS 13, cámara trasera doble de 12MP, resistente al agua y batería de larga duración - Blanco"
    },
    "disponibilidad": [
        {
            "centro": "C104",
            "nodo": false,
            "codigo_nodo": "C201",
            "ubicacion": {
                "codigo_municipio": "999",
                "departamiento": "999",
                "latitude": "0000000000",
                "longitude": "0000000000"
            }
        },{
            "centro": "C105",
            "nodo": false,
            "codigo_nodo": "C201",
            "ubicacion": {
                "codigo_municipio": "999",
                "departamiento": "999",
                "latitude": "0000000000",
                "longitude": "0000000000"
            }
        },{
            "centro": "C201",
            "nodo": false,
            "codigo_nodo": "C201",
            "ubicacion": {
                "codigo_municipio": "999",
                "departamiento": "999",
                "latitude": "0000000000",
                "longitude": "0000000000"
            }
        }
    ]
})

db.stocks.insert({
    "material": {
        "sku": "100203",
        "nombre": "IPHONE 256 BRANCO",
        "description": "Apple iPhone 11 con 64 GB, pantalla Retina HD de 6.1, iOS 13, cámara trasera doble de 12MP, resistente al agua y batería de larga duración - Blanco"
    },
    "disponibilidad": [
        {
            "centro": "C106",
            "nodo": false,
            "codigo_nodo": "C202",
            "ubicacion": {
                "codigo_municipio": "999",
                "departamiento": "999",
                "latitude": "0000000000",
                "longitude": "0000000000"
            }
        },{
            "centro": "C202",
            "nodo": false,
            "codigo_nodo": "C202",
            "ubicacion": {
                "codigo_municipio": "999",
                "departamiento": "999",
                "latitude": "0000000000",
                "longitude": "0000000000"
            }
        }
    ]
})
