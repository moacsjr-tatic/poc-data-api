# Eve 	
  
FROM python:3.8
ARG BUILD_DATE
ARG VCS_REF
ARG VERSION
LABEL org.label-schema.build-date=$BUILD_DATE \
    org.label-schema.name="Docker Eve" \
    org.label-schema.description="Docker image for Python Eve" \
    org.label-schema.url="https://github.com/alekspankov/docker-eve-python" \
    org.label-schema.vcs-ref=$VCS_REF \
    org.label-schema.vcs-url="https://github.com/alekspankov/docker-eve-python" \
    org.label-schema.vendor="Alexander Pankov" \
    org.label-schema.version=$VERSION \
    org.label-schema.schema-version="1.0"
LABEL maintainer="Alexander Pankov <ap@wdevs.ru>"

WORKDIR /usr/src/app

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

ENV AWS_ACCESS_KEY_ID="AKIAIASZEDK6LN7CLC6Q"
ENV AWS_SECRET_ACCESS_KEY="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"
ENV FLASK_ENV="development"

COPY ./app/* ./

CMD ["python", "./app.py", "--log=DEBUG"]
