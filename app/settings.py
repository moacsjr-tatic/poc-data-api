#MONGO_HOST="127.0.0.1"
MONGO_HOST="mongo"
MONGO_PORT=27017
MONGO_DBNAME="inventario"
MONGO_USERNAME="oms"
MONGO_PASSWORD="omnicanal"

RESOURCE_METHODS = ['GET', 'POST', 'DELETE']
ITEM_METHODS = ['GET', 'PATCH', 'PUT', 'DELETE']
X_DOMAINS='*'
X_HEADERS='Content-Type,Authorization'
DATE_FORMAT='%Y-%m-%dT%H:%M:%S%z'
HATEOAS = False
#ID_FIELD='id'

inventario_schema = {
    # 'title' tag used in item links. Defaults to the resource title minus
    # the final, plural 's' (works fine in most cases but not for 'people')
    'item_title': 'stock',
    'schema': { 
        'material': {
            'type': 'dict',
            'schema': {
                #
                'sku': { 'type' : 'string' },
                'nombre': { 'type' : 'string' },
                'description' : { 'type' : 'string' },
                'caracteristicas': {
                    'type': 'list',
                    'schema': {
                        'type': 'dict',
                         'schema': {
                            'nombre': { 'type': 'string'},
                            'valor': { 'type': 'string'}
                          }
                    }
                }
            }
        },
        'disponibilidad': { 
            'type': 'list',
            'schema': {
              'type': 'dict',
              'schema': {
                #
                'centro': { 'type' : 'string' },
                'nodo': { 'type' : 'boolean' },
                'codigo_nodo' : { 'type' : 'string' },
                'tiempo_entrega' : { 'type' : 'string' },
                'ubicacion': {
                    'type': 'dict',
                    'schema': {
                        'codigo_municipio': { 'type': 'string'},
                        'departamiento': { 'type': 'string'},
                        'longitude': { 'type': 'string'},
                        'latitude': { 'type': 'string'},
                    }
                }
              }
            }
        }
    }
}


DOMAIN = {'stocks': inventario_schema}

